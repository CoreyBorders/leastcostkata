package co.coreyborders;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class FileReader {
	public void readFile() throws FileNotFoundException {
		try (Scanner fileInput = new Scanner(new File("input.txt"))) {
			List<List<Integer>> lists = new ArrayList<List<Integer>>();

			while (fileInput.hasNextLine()) {
				String line = fileInput.nextLine();
				List<String> numString = new ArrayList<String>(Arrays.asList(line.split(" ")));
				List<Integer> intList = new ArrayList<Integer>();
				for(String num : numString) {
					int numberAsInt = Integer.parseInt(num);
					intList.add(numberAsInt);
				}
				lists.add(intList);
			}
				
			PathCalc pathCalc = new PathCalc();
			pathCalc.findLowCostPath(lists);
			
		}
	}
}
