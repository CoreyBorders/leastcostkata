package co.coreyborders;

import java.util.ArrayList;
import java.util.List;

public class PathCalc {
	private static int column = 0;
	private static int row;
	private static List<Integer> path;
	private static List<Integer> nextOptions;
	private static int cost = 0;

	void findLowCostPath(List<List<Integer>> lists) {
		List<Integer> findLowestStart = new ArrayList<Integer>();
		for (int row = 0; row < lists.size(); row++) {
			int num = lists.get(row).get(column);
			findLowestStart.add(num);
		}
		int minValue = getMinValue(findLowestStart);
		path = new ArrayList<Integer>();
		path.add(minValue);
		cost += minValue;
		column++;
		
		for(int i = 1; i < lists.get(0).size(); i++) {
			findNextLowestVal(lists, row);
			minValue = getMinValue(nextOptions);
			path.add(minValue);
			cost += minValue;
			column++;
		}

		if(cost > 50) {
			System.out.println("No");
		} else {
			System.out.println("Yes");
		}
		System.out.println("Cost: " + cost);
		System.out.println("Path: " + path);
	

	}

	public static int getMinValue(List<Integer> findLowestStart) {
		int minValue = findLowestStart.get(0);
		for (int i = 1; i < findLowestStart.size(); i++) {
			if (findLowestStart.get(i) < minValue) {
				minValue = findLowestStart.get(i);
				row = i;
			}
		}
		return minValue;
	}


	private List<Integer> findNextLowestVal(List<List<Integer>> lists, int row) {
		nextOptions = new ArrayList<Integer>();
		if (row >= 1) {
			for (int i = -1; i <= 1; i++) {
				nextOptions.add(lists.get(row + i).get(column));
			}
		} else {
			for (int i = 0; i <= 1; i++)
				nextOptions.add(lists.get(row + i).get(column));
		}
		return nextOptions;
	}

}
